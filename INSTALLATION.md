## Installation / Development

Requirements:
* Git
* Python >= 3.6 (on linux python3-venv and python3-dev packages should be installed)
* PostgreSQL >= 10


**Installation**

Clone the git repository

```
git clone git@gitlab.com:sambasan/genresearch.git
```

Create virtualenv for project and install requirements

```
cd genresearch
python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements/requirements_local.txt
```

(Optional) Create new postgresql role and database

```
CREATE ROLE genresearch WITH LOGIN ENCRYPTED PASSWORD '{password}';
CREATE DATABASE genresearch WITH OWNER genresearch;
```

If no postgresql database is made and configured via dynaconf, the project will use sqlite3 automatically (minimal mode)

Create `.secrets.toml` file to project root with following contents:

```
[default]
SECRET_KEY = 'change me'
```

It is also possible to override the test database settings in the secrets file like this:

```
DATABASES__default__ENGINE = 'django.db.backends.sqlite3'
```

Run migrations and start server (development)

```
python manage.py migrate
python manage.py runserver 127.0.0.1:8000
```