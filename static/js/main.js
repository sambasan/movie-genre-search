/**
 * Web GUI search service handler
 */
/* global gettext, Urls */

/**
 *  on document ready
 */
document.addEventListener("DOMContentLoaded", function() {
    let search_form = document.querySelector('.form-search');

    search_form.addEventListener('submit', function(event) {
        event.preventDefault();
        let genre = document.querySelector('#input-search').value;
        // Set spinner on and change text
        let search_button_text = $('.search-btn-text');
        search_button_text.text(gettext('Searching...'));
        let search_btn_spinner = $('.search-spinner');
        search_btn_spinner.removeClass('d-none');
        $('#search-results').empty();

        $.get(Urls['core:search-api-list'](), {
            'genre': genre
        }).done(function(data) {
            $.each(data, function(index, movie) {
                let row_el = $('<li class="list-group-item d-flex justify-content-between align-items-center"></li>');
                let movie_title_el = $('<span></span>').text(movie.title);
                let movie_year_el = $('<span class="badge badge-primary badge-pill"></span>').text(movie.year);
                row_el.append(movie_title_el, movie_year_el);
                $('#search-results').append(row_el);
            });

        }).fail(function(xhr, status, error) {
            alert(gettext('Error loading results'));
        }).always(function(){
            // Set spinner on and change text
            search_button_text.text(gettext('Search'));
            search_btn_spinner.addClass('d-none');
        })
    })
});