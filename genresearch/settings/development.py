"""
Development settings for genresearch
Provides additional settings required by development environment
Based on base settings
"""

from .base import *  # noqa

DEBUG = True
TEMPLATES[0]['OPTIONS']['debug'] = DEBUG

# Development utilizes sqlite3 db, but it can be customized here or by dynaconf files or environment variables

# HERE STARTS DYNACONF EXTENSION LOAD (Keep at the very bottom of settings.py)
# Read more at https://dynaconf.readthedocs.io/en/latest/guides/django.html
import dynaconf  # noqa

settings = dynaconf.DjangoDynaconf(__name__)  # noqa
# HERE ENDS DYNACONF EXTENSION LOAD (No more code below this line)
