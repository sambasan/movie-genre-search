"""
Build settings for genresearch
Provides additional settings required by build environment
"""

from .base import *  # noqa

# Make sure debug is on to catch all errors
DEBUG = True
TEMPLATES[0]['OPTIONS']['debug'] = DEBUG
ALLOWED_HOSTS = ['testserver']  # Allow only api clients to connect to instance

INSTALLED_APPS.extend([
    'django_probes'  # Provide management command to wait for main database, useful in pipelines
])

# Database pre-setup for build environment
# https://docs.djangoproject.com/en/3.1/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'genresearch',
        'HOST': 'postgres',
        'PORT': '5432',
        'USER': 'genresearch',
    },
}

# HERE STARTS DYNACONF EXTENSION LOAD (Keep at the very bottom of settings.py)
# Read more at https://dynaconf.readthedocs.io/en/latest/guides/django.html
import dynaconf  # noqa

settings = dynaconf.DjangoDynaconf(__name__)  # noqa
# HERE ENDS DYNACONF EXTENSION LOAD (No more code below this line)
