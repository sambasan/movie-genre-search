"""
Production (WIP) settings for genresearch
Provides additional settings required by production environment
Based on base settings
"""

from .base import *  # noqa

# Force debug false on production
DEBUG = False
TEMPLATES[0]['OPTIONS']['debug'] = DEBUG

# TODO: do production checks and add required security parameters before deploy
#       https://docs.djangoproject.com/en/3.1/howto/deployment/checklist/#run-manage-py-check-deploy

# HERE STARTS DYNACONF EXTENSION LOAD (Keep at the very bottom of settings.py)
# Read more at https://dynaconf.readthedocs.io/en/latest/guides/django.html
import dynaconf  # noqa

settings = dynaconf.DjangoDynaconf(__name__)  # noqa
# HERE ENDS DYNACONF EXTENSION LOAD (No more code below this line)
