## Movie Genre Search

Movie Genre Search is a simple demo application to search and suggest movies to user by genre. (not ready for production in this state)

The application features a simple web based user interface (HTML, CSS, JS) and a REST API to search the movies by genre.
Static files are provided from CDN but could be provided from project static as well.

The results are fetched currently straight from other third party sites such as IMDB (example in this case) utilizing different service adapters. (see `core.search_adapters` and `core.api`)
There is currently working implementation of IMDB adapter and a Fake example adapter provided.

Application structure is meant to simplify adding more service adapters in the future.

Thoughts on future architecture changes towards microservices can be found in section [Future development](#future-development)

Movie Genre Search runs as a web application on Django Python framework with an optional PostgreSQL database for user authentication / data persistence.
Project requirements can be seen from `requirements_{environment}.txt` files.

### Screenshots
![architecture](docs/webgui.png)
![architecture](docs/webgui_searched.png)

### Health status

[![pipeline status](https://gitlab.com/sambasan/movie-genre-search/badges/master/pipeline.svg)](https://gitlab.com/sambasan/movie-genre-search/-/commits/master)

[![coverage report](https://gitlab.com/sambasan/movie-genre-search/badges/master/coverage.svg)](https://gitlab.com/sambasan/movie-genre-search/-/commits/master)

#### Project Structure

The project structure uses basic Django application structure mostly with multiple environment based settings levels and separated secrets file.

The project root contains only required files such as project requirements, django management file and .gitignore.

The main application that contains root settings, wsgi and asgi endpoints and main url config is the same as project `genresearch`

Core application exists to provide a common API for possible other applications in the project (currently there are none)

**Architecture**

Uses Django MVT (Model-View-Template) architecture with additional common genre search API and search adapters for every service such as Netflix and IMDB.
The common API binds all search adapters together through registry and aggregates every service results to one.

**Requirements**

See: `requirements/requirements_{environment}.txt` files.

**Core**

The main beef. Has search view, REST API, abstracts and search adapters and their unit and integration tests.

**REST API**

Resides in `core\rest_views.py`, hooked up to main application urls through core url conf.
Uses Django Rest Framework in simple mode without Serializers.

**Views**

Normal HTML/CSS/JS views are located in: `core\views.py` and use django/html templates from main application template directory.

**URLs**

Core application has own urls.py that is included in the main application url conf.

**Templates**

Templates are on root project level and are organized per application.

**Tests**

Core application has unit tests for real search adapters and mocked integration tests for views and REST API.

#### Authentication and Security

Currently no authentication is utilized. In future the authentication between the web gui and rest api could be provided via JWT or oAuth2 for example.
Additionally throttling could be enabled to slow down anonymous usage of the API.

#### REST API

REST API is available on URL: `/api/search-by-genre`.

`genre` is a required get parameter for the API.

Few usage examples:
```
GET /api/search-by-genre/
HTTP 400 Bad Request
Allow: GET, HEAD, OPTIONS
Content-Type: application/json
Vary: Accept

{
    "msg": "No genre provided"
}
```

```
GET /api/search-by-genre/?genre=science
HTTP 200 OK
Allow: GET, HEAD, OPTIONS
Content-Type: application/json
Vary: Accept

[
    {
        "title": "12 Monkeys",
        "year": 1995
    },
    {
        "title": "Ammonite",
        "year": 2020
    },
    {
        "title": "Annihilation",
        "year": 2018
    },
    {
        "title": "Barbarella",
        "year": 1968
    },
    {
        "title": "Blade Runner 2049",
        "year": 2017
    },
    ... snip ...
]
```

Django Rest Framework also provides a browsable self describing API to use if enabled.
It is also possible to provide separate schema in machine or human readable formats from the code using tools such as Swagger.

### Future development <a name="future-development"></a>

**Architecture**

When future production deployment is considered. I would utilize the following changes to the architecture to allow better scaling and possible change to microservices architecture:

1. Split web UI from rest API - The web ui would mostly utilize the REST API and can function on its own.
2. Build additional API gateway in front of different REST API:s, such as authentication and movie search results.
3. Scale UI instances and API instances horizontally, load balance between them
4. Utilize common cache and persistence layer between the instances (movie data, auth should be separate)
5. Some movie data sources such as Netflix would possibly need scraping, this would be done via background tasks utilizing Celery or other background task framework.
6. Redis could work as a cache layer and as a communication gateway for background tasks
7. Background tasks could use timed or event based workflow
8. In extreme situation, every adapter could be its own service technically with their own db and caches, we could then use aggregator pattern to collect data from them.

These changes are mostly made to allow more effective scaling from each part of the architecture in big deployments.
In smaller deployments the current architecture could work.

The end result would look something like this:
![architecture](docs/architecture.png)

**Testability**

I would use unit and integration tests with mocking to build a stable API without real requirement to different libraries and services.
Generic test with schema validation could be utilized to automatically pick up new real adapters for testing later. (example provided with registry and generic unit test)
Code style, syntax, linting and coverage (for all used languages) should also be a part of the automated test suite.
Whole test suite should be runnable on developers computers and in CI/CD environment such as GitLab pipeline or Jenkins etc.
A continuous integration should be set up. Currently this project utilizes GitLab pipeline as an example.

**Persistence**

Considering persistence, based on the deployment size, different layers would be needed for each type of data:
1. Users / authentication / permissions: PostgreSQL or another service
2. Movie search results (scrapes / adapter fetched data): PostgreSQL / ElasticSearch / NoSQL DB such as Mongo
3. Movie search results and generic cache / background task queue: Redis
4. Secret storage / per tenant secrets: Vault / Consul
5. App metrics / Monitoring / Logs: ElasticSearch

If the application would be used as a SaaS for many customers it would be feasible to utilize multi-tenancy to further divide the persistence needs and allow better scaling.

**Scaling**

The application should scale well horizontally because in its current form (if django were to be configured to run without db at all) it is stateless. It would become stateful if we were to use auth with sessions but that can be avoided using token authentication.
If background tasks were utilized to keep the database up to date using the adapters we could further divide the workload horizontally.
The data persistence layer could also be scaled horizontally.
UI and API separation would also enhance horizontal scaling as well as adding load balancers.

Vertical scaling is possible as well but horizontal scaling would be preferable in this case as we can benefit from additional workers and application instances. Vertical scaling also would cap out at some point since one application instance can utilize only so many resources.

**Features**

Additional features for future:
* Duplicate movie handling from different adapters
* Background search jobs from different APIs (or scraping)
* Search data persistence
* Additional information from the movies (year, studio, actors, rating), sorting, pagination
* More search adapters

**Security**

Django has very good built in pluggable authentication with groups and permissions support so it would be feasible to utilize it, but it is also simple to add any other, possibly SSO supporting auth framework to it that also works on the REST API side.
Possible options include: oAuth2 and JWT for example.

Security regarding third party libraries could be handled via build step such as: [Safety](https://github.com/pyupio/safety)

Own code security could be checked via bandit tool: [Bandit](https://bandit.readthedocs.io/en/latest/)

Production settings file should be modified to work with TLS etc.

### Development & Running

**Installation**

For installation instructions see `INSTALLATION.md`

**Tests**

You can run tests by issuing the following command after installation:
```
pytest -sv  # Run tests on the current virtualenv
# OR
tox  # Run tests on multiple python or django versions at once
```

### History

Initial version
* Basic project structure and settings
* Simple web based user interface
* REST API base
* Pure JS handler on web based user interface to work with the REST API
* Tests for current features and base test for additional adapters
* Documentation
