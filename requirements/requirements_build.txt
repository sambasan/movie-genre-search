# Build requirements

-r ./requirements_base.txt

django-probes==1.2.0  # Provides wait for database function for GitLab Pipeline
coverage==5.3.1
isort==5.6.4
mccabe==0.6.1
mock==4.0.3
packaging==20.8
pip-licenses==3.2.0
pycodestyle==2.6.0
pyflakes==2.2.0
pylint==2.6.0
pyparsing==2.4.7
pytest==6.2.1
pytest-cov==2.10.1
pytest-django==4.1.0
pytest-flake8==1.0.7
pytest-pylint==0.18.0
tox==3.20.1