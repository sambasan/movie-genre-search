from core.search_adapters import SearchAdapterRegistry


class GenreSearchAPI:
    """
    Generic api to provide single point of entry for getting results from search adapters

    Currently used from rest viewset only
    """

    def __init__(self, *args, **kwargs):
        self.search_adapter_registry = SearchAdapterRegistry

    def search_by_genre(self, genre):
        """
        Search all enabled adapters for movies
        TODO: Duplicate handling
        :param genre: genre as a string
        :return: movies in the schema described in MovieSchema
        """
        movies = []
        for _, adapter_class in self.search_adapter_registry.get_registry().items():
            adapter = adapter_class()
            data = adapter.search_by_genre(genre)
            for movie in data:
                movies.append(movie)

        return movies
