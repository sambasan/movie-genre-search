import abc


class Registry(type, metaclass=abc.ABCMeta):
    """
    Abstract registry type metaclass that automatically registers subclasses to it

    implementations with static variable:
    abstract = True
    will not be added to registry
    """

    def __new__(mcs, name, bases, attrs):
        new_cls = super().__new__(mcs, name, bases, attrs)
        if not hasattr(mcs, '_registry'):
            mcs._registry = {}
        else:
            if not (hasattr(new_cls, 'abstract') and new_cls.abstract):
                mcs._registry[new_cls.__name__] = new_cls
        return new_cls

    @classmethod
    def get_registry(mcs):
        return dict(mcs._registry)

    @classmethod
    def as_choices(mcs):
        """
        Produces a choices list from registry entries

        key is the python classname and value is the class verbose name
        """
        return ((cn, c.verbose_name) for cn, c in mcs.get_registry().items())
