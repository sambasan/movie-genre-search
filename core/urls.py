from django.urls import include, path
from rest_framework import routers

from core.rest_views import SearchByGenreViewSet
from core.views import GenreSearchView

app_name = 'core'

router = routers.DefaultRouter()
router.register(r'search-by-genre', SearchByGenreViewSet, basename='search-api')

urlpatterns = [
    path('', GenreSearchView.as_view(), name='search'),
    path('api/', include(router.urls), name='rest-api')
]
