from imdb import IMDb
from marshmallow import Schema, fields, EXCLUDE

from core.abstract import Registry
from django.utils.translation import gettext_lazy as _


class SearchAdapterRegistry(Registry):
    """Implementation of registry for search adapters"""
    pass


class MovieSchema(Schema):
    title = fields.Str()
    year = fields.Int()
    # source = fields.Str()  # Contains the source adapter class name
    # source_id = fields.Str()  # Contains the source id such as IMDB movieID

    class Meta:
        unknown = EXCLUDE


class SearchAdapterBase(metaclass=SearchAdapterRegistry):
    """
    Base class for movie search adapters.
    Provides simple adapted way to get search results from different libraries and APIs.
    Hybrid of facade and adapter since search adapters can handle the lifecycle of library API clients as well.

    Usually holds one instance of the API client at runtime and translates the requests for it

    API clients should be initialized in __init__ method
    search_by_genre method should be implemented by subclasses
    verbose_name should be set to provide sensible name
    """

    def __init__(self, *args, **kwargs):
        self.schema = MovieSchema()

    def search_by_genre(self, genre):
        """
        Search movies by genre
        :param genre: movie genre to search for (str)
        :return: list of movie objects, see MovieSchema for schema inside the list
        """
        raise NotImplementedError('Subclasses must implement this method')


class IMDBSearchAdapter(SearchAdapterBase):
    """
    IMDB search adapter

    Utilizes IMDbPY and IMDB API for searches

    For IMDbPY usage see: https://imdbpy.readthedocs.io/en/latest/

    TODO: error handling
    TODO: sorting
    """
    verbose_name = _('IMDB search adapter')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.client = IMDb()

    def search_by_genre(self, genre):
        """
        IMDbPY does not provide proper genre search functionality
        Use keyword search as closest equivalent to genre search for proof of concept
        In future this could use top movies and filter them by genre for example
        :param genre: genre str to search for
        :return: list of movies (currently capped to first page containing 50 movies that match the keyword)
        """
        # Use keyword search as closest equivalent to genre search
        # In future this could use top movies and filter them by genre for example
        # IMDB keywords must be in lower case
        genre = genre.lower()
        movies = []
        for movie in self.client.get_keyword(genre):
            # Schema is used for filtering unused data in this case, but could be used for validation as well
            data = self.schema.dump(movie.data)
            movies.append(data)
        return movies


class FakeSearchAdapter(SearchAdapterBase):
    """
    Fake search adapter to work as an example when we don't know about the external services yet
    """

    verbose_name = _('Fake search adapter')

    movies = [
        {
            'title': '!A test movie',
            'year': 2021
        }
    ]

    def search_by_genre(self, genre):
        """Return same results for every genre"""
        return self.movies
