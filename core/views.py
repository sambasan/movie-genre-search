from django.views.generic import TemplateView


class GenreSearchView(TemplateView):
    template_name = 'core/genre_search.html'
