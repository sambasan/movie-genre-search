from rest_framework import permissions
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet
from rest_framework import status

from core.api import GenreSearchAPI


class SearchByGenreViewSet(ViewSet):
    """
    REST API viewset to provide searching by genre from all adapters using GenreSearchAPI.
    Uses Django Rest Framework default json format for output.
    Does not require any permissions or authentication currently.

    `genre` is a required get parameter
    """

    # Disable authentication
    permission_classes = [permissions.AllowAny]

    def list(self, request):
        """
        Default get operation from this viewset provides the search functionality
        :return: list of movie objects based on search_adapters.MovieSchema
        """

        genre = self.request.GET.get('genre', None)
        # Return bad request if no required parameter genre is provided
        if genre is None:
            return Response({
                'msg': 'No genre provided'
            }, status=status.HTTP_400_BAD_REQUEST)

        gsa = GenreSearchAPI()
        results = gsa.search_by_genre(genre)

        # TODO: Do optional ordering and filtering here
        # Currently ordered by title alphabetically
        results = sorted(results, key=lambda t: t['title'])
        return Response(results)
