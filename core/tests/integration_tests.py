from unittest.mock import patch

from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase


class GenreSearchViewTestCase(TestCase):
    """
    Tests for the main genre search view
    """

    view_name = 'core:search'

    def test_genre_search_view(self):
        """Simple smoke test to see if the view opens without http status code errors"""
        response = self.client.get(reverse(self.view_name))
        self.assertEqual(response.status_code, 200)


class GenreSearchAPITestCase(APITestCase):
    """
    Tests for the main genre search REST API
    Tests REST API viewset, generic api and existing search adapters
    """

    viewset_name = 'core:search-api-list'

    mock_data = [
        {
            'title': 'mock movie',
            'year': 9999
        }
    ]

    def test_genre_search_api(self):
        """End to end testcase with mocked search adapter results"""
        url = reverse(self.viewset_name)
        data = {'genre': 'scifi'}
        # Mock the IMDBSearchAdapter function to return none
        with patch('core.search_adapters.IMDBSearchAdapter.search_by_genre', return_value=self.mock_data) as mock_func:
            response = self.client.get(url, data, format='json')
            # Make sure the whole request cycle was OK
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            # Make sure that our API called the mocked adapter method OK
            mock_func.assert_called_once_with('scifi')
            # Make sure that the response still has the fake adapter data
            response_movie_titles = [movie['title'] for movie in response.data]
            self.assertTrue('mock movie' in response_movie_titles)
            # Make sure that our fake ui adapter still provided its results to api as well
            self.assertTrue('!A test movie' in response_movie_titles)
