from django.test import SimpleTestCase

from core.search_adapters import SearchAdapterRegistry


class GenericSearchAdapterTestCase(SimpleTestCase):
    """Generic unit test base for search adapters"""

    def setUp(self):
        self.search_adapter_registry = SearchAdapterRegistry

    def test_search_by_genre_smoke(self):
        search_adapters = self.search_adapter_registry.get_registry()
        for _, adapter_class in search_adapters.items():
            adapter = adapter_class()
            results = adapter.search_by_genre('scifi')
            self.assertTrue(results)

    def test_search_by_genre_schema(self):
        search_adapters = self.search_adapter_registry.get_registry()
        for _, adapter_class in search_adapters.items():
            adapter = adapter_class()
            results = adapter.search_by_genre('scifi')
            self.assertTrue(results)
